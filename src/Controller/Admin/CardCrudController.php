<?php

namespace App\Controller\Admin;

use App\Entity\Card;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CardCrudController extends AbstractCrudController
{

    public const ACTION_DUPLICATE = 'duplicate';
    public const TEST_BASE_PATH = 'uploads/cards';
    public const TEST_UPLOAD_PATH = 'public/' . self::TEST_BASE_PATH;

    public static function getEntityFqcn(): string
    {
        return Card::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            ImageField::new('locateNamee')
                ->setBasePath(self::TEST_BASE_PATH)
                ->setUploadDir(self::TEST_UPLOAD_PATH)
                ->setUploadedFileNamePattern('[randomhash].[extension]')
                ->setRequired(false),   
        ];
    }
    
}
